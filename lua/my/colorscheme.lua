local colorscheme = "nordfox"

vim.g.sonokai_style = "maia"
vim.g.sonokai_better_performance = 1
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
	vim.notify("colorscheme " .. colorscheme .. " not found!")
	return
end
