local status_ok, null_ls = pcall(require, "null-ls")
if not status_ok then
	return
end
local formatting = null_ls.builtins.formatting
local code_actions = null_ls.builtins.code_actions
local diagnostics = null_ls.builtins.diagnostics
null_ls.setup({
	sources = {
		formatting.black.with({ extra_args = { "--skip-string-normalization" } }),
		formatting.stylua,
		formatting.eslint,
		diagnostics.eslint,
		code_actions.refactoring,
	},
	on_attach = function(client)
		if client.resolved_capabilities.document_formatting then
			-- vim.cmd([[
			--          augroup LspFormatting
			--              autocmd! * <buffer>
			--              autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()
			--          augroup END
			--          ]])
		end
	end,
})

local status_ok, refactoring = pcall(require, "refactoring")
if not status_ok then
	return
end
refactoring.setup({})
