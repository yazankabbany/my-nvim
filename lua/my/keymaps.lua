local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

-- Shorten function name
local keymap = vim.api.nvim_set_keymap

--Remap space as leader key
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-- TODO use custom function for each mode

-- Normal --
-- Better window navigation
keymap("n", "<C-h>", "<C-w>h", opts)
keymap("n", "<C-j>", "<C-w>j", opts)
keymap("n", "<C-k>", "<C-w>k", opts)
keymap("n", "<C-l>", "<C-w>l", opts)

-- Resize with arrows
keymap("n", "<C-Up>", ":resize +2<CR>", opts)
keymap("n", "<C-Down>", ":resize -2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

-- Better EOL navigation
keymap("n", "L", "$", opts)
keymap("n", "H", "^", opts)

-- Better buffer navigation
keymap("n", "<tab>", ":bnext<CR>", opts)
keymap("n", "<S-tab>", ":bprevious<CR>", opts)

-- Telescope
keymap("n", "<leader>s", "<cmd>Telescope live_grep<cr>", opts)
keymap("n", "<leader><leader>", "<CMD>lua require'my.telescope'.project_files()<CR>", opts)
keymap("n", "<leader>b", "<cmd>Telescope buffers<cr>", opts)
keymap("n", "<Leader>gb", "<cmd>Telescope git_branches<cr>", opts)
keymap(
	"n",
	"<leader>ca",
	":lua require'telescope.builtin'.lsp_code_actions(require('telescope.themes').get_cursor({ winblend = 10 }))<cr>",
	opts
)
keymap(
	"v",
	"<leader>ca",
	":lua require'telescope.builtin'.lsp_code_actions(require('telescope.themes').get_cursor({ winblend = 10 }))<cr>",
	opts
)
-- NvimTree
keymap("n", "<c-t>", "<CMD>:NvimTreeToggle<CR>", opts)
keymap("n", "<leader>tf", "<CMD>:NvimTreeFindFile<CR>", opts)

-- Misc
keymap("n", "<Leader>mt", ":vs ~/Documents/todo.txt<CR>", opts)
keymap("n", "<CR> ", ":noh<CR><CR>:<backspace>", opts) -- remove search highlight on <CR>

-- Insert --

-- Visual --
-- Move text up and down
keymap("v", "<A-j>", ":m .+1<CR>==", opts)
keymap("v", "<A-k>", ":m .-2<CR>==", opts)
keymap("v", "p", '"_dP', opts) -- replace text does not override clipboard

-- Better EOL navigation
keymap("n", "L", "$", opts)
keymap("n", "H", "^", opts)

-- Visual Block --

-- Terminal --
-- Better terminal navigation
keymap("t", "<C-h>", "<C-\\><C-N><C-w>h", term_opts)
keymap("t", "<C-j>", "<C-\\><C-N><C-w>j", term_opts)
keymap("t", "<C-k>", "<C-\\><C-N><C-w>k", term_opts)
keymap("t", "<C-l>", "<C-\\><C-N><C-w>l", term_opts)
