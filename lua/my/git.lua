local opts = { noremap = true, silent = true }

local term_opts = { silent = true }

local keymap = vim.api.nvim_set_keymap

-- Git fugitive
keymap("n", "<Leader>gs", ":G<CR>", opts)
keymap("n", "<Leader>gp", ":G pull<CR>", opts)
keymap("n", "<Leader>gh", ":G push<CR>", opts)
keymap("n", "<Leader>gP", ":G push<CR>", opts)
keymap("n", "<Leader>gj", ":diffget //2<CR>", opts)
keymap("n", "<Leader>gk", ":diffget //3<CR>", opts)
